/*
 * Copyright (C) 2021 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package es.elixir.bsc.ecotools.frontend.repo;

import org.eclipse.jgit.errors.CorruptObjectException;
import org.eclipse.jgit.lib.Config;
import org.eclipse.jgit.storage.file.FileBasedConfig;
import org.eclipse.jgit.util.FS;
import org.eclipse.jgit.util.SystemReader;

/**
 * @author Dmitry Repchevsky
 */

public class JgitSytemReaderHack extends SystemReader {

    private final SystemReader sr;

    public JgitSytemReaderHack(SystemReader sr) {
        this.sr = sr;
    }

    @Override
    public void checkPath(String path) throws CorruptObjectException {
    }

    @Override
    public void checkPath(byte[] path) throws CorruptObjectException {
    }

    @Override
    public String getHostname() {
        return sr.getHostname();
    }

    @Override
    public String getenv(String arg0) {
        return sr.getenv(arg0);
    }

    @Override
    public String getProperty(String arg0) {
        return sr.getProperty(arg0);
    }

    @Override
    public FileBasedConfig openUserConfig(Config arg0, FS arg1) {
        return sr.openUserConfig(arg0, arg1);
    }

    @Override
    public FileBasedConfig openSystemConfig(Config arg0, FS arg1) {
        return sr.openSystemConfig(arg0, arg1);
    }

    @Override
    public long getCurrentTime() {
        return sr.getCurrentTime();
    }

    @Override
    public int getTimezone(long arg0) {
        return sr.getTimezone(arg0);
    }
}
