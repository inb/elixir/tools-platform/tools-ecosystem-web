/*
 * Copyright (C) 2021 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package es.elixir.bsc.ecotools.frontend.repo;

/**
 * @author Dmitry Repchevsky
 */

public enum ToolProvider {
    BIOTOOLS("", ".biotools.json"),
    OPENEBENCH("", ".oeb.metrics.json"),
    GALAXY("", ".galaxy.yaml"),
    BIOCONDA("bioconda_", ".yaml"),
    BIOCONTAINERS("", ".biocontainers.yaml"),
    BIOIMAGE("", ".neubias.raw.json"),
    BIOSCHEMAS("", ".bioschemas.jsonld"),
    DEBIAN("", ".debian.yaml");
    
    public final String PREFIX;
    public final String SUFFIX;

    ToolProvider(final String prefix, final String suffix) {
        PREFIX = prefix;
        SUFFIX = suffix;
    }
    
    public String getName() {
        return name();
    }
}
