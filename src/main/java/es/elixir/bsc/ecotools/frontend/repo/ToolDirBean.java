/*
 * Copyright (C) 2021 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package es.elixir.bsc.ecotools.frontend.repo;

import com.google.common.base.Charsets;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.json.Json;
import javax.json.JsonReader;
import javax.json.JsonValue;

/**
 * @author Dmitry Repchevsky
 */

public class ToolDirBean {
    
    private final Path dir;
    private volatile WeakReference<String> description;

    public ToolDirBean(final Path dir) {
        this.dir = dir;
    }
    
    public String getName() {
        return dir.getFileName().toString();
    }

    public String getUri() {
        return Configuration.HTTP_PATH + dir.toString();
    }

    public String getDescription() {
        String value = null;

        if (description != null) {
            value = description.get();
            if (value != null) {
                return value;
            }
        }
        try (DirectoryStream<Path> stream = Files.newDirectoryStream(
                dir, (Path file) -> Files.isRegularFile(file, LinkOption.NOFOLLOW_LINKS))) {
            for (Path file : stream) {
                final String name = file.getFileName().toString();
                if (name.endsWith(ToolProvider.BIOTOOLS.SUFFIX)) {
                    try (JsonReader reader = Json.createReader(Files.newBufferedReader(file, Charsets.UTF_8))) {
                        final JsonValue json = reader.readValue();
                        if (json.getValueType() == JsonValue.ValueType.OBJECT) {
                            value = json.asJsonObject().getString("description", null);
                            if (value != null && !value.isEmpty()) {
                                break;
                            }
                        }
                    }
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(ToolDirBean.class.getName()).log(Level.SEVERE, null, ex);
        } 
        
        description = new WeakReference(value);
        
        return value;
    }
    
    public Collection<String> getProviders() {
        final Set<String> providers = new TreeSet();
        try (DirectoryStream<Path> stream = Files.newDirectoryStream(
                dir, (Path file) -> Files.isRegularFile(file, LinkOption.NOFOLLOW_LINKS))) {
            for (Path file : stream) {
                final String name = file.getFileName().toString();
                for (ToolProvider provider : ToolProvider.values()) {
                    if (name.endsWith(provider.SUFFIX)) {
                        providers.add(provider.name());
                    }
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(ToolDirBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return providers;
    }
    
    public boolean anyMatch(List<String> providers) {
        final List<ToolProvider> list = providers.stream()
                .map(value -> ToolProvider.valueOf(value))
                .collect(Collectors.toList());
        try (DirectoryStream<Path> stream = Files.newDirectoryStream(
                dir, (Path file) -> Files.isRegularFile(file, LinkOption.NOFOLLOW_LINKS))) {
            for (Path file : stream) {
                final String name = file.getFileName().toString();
                for (ToolProvider provider : list) {
                    if (name.startsWith(provider.PREFIX) && name.endsWith(provider.SUFFIX)) {
                        return true;
                    }
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(ToolDirBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
    public boolean allMatch(List<String> providers) {
        final List<ToolProvider> list = providers.stream()
                .map(value -> ToolProvider.valueOf(value))
                .collect(Collectors.toList());
        try (DirectoryStream<Path> stream = Files.newDirectoryStream(
                dir, (Path file) -> Files.isRegularFile(file, LinkOption.NOFOLLOW_LINKS))) {
            for (Path file : stream) {
                final String name = file.getFileName().toString();
                for (Iterator<ToolProvider> iter = list.iterator(); iter.hasNext();) {
                    final ToolProvider provider = iter.next();
                    if (name.startsWith(provider.PREFIX) && name.endsWith(provider.SUFFIX)) {
                        iter.remove();
                        break;
                    }
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(ToolDirBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list.isEmpty();
    }
}
