/*
 * Copyright (C) 2021 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package es.elixir.bsc.ecotools.frontend.repo;

import com.google.common.jimfs.Jimfs;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.FileSystem;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Stream;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.PullResult;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.util.SystemReader;

/**
 * @author Dmitry Repchevsky
 */

@ApplicationScoped
@Named("repo")
public class GitRepoBean {
    
    private Path root;
    
    private volatile long last_sync;
    
    @PostConstruct
    public void init() {
        
        SystemReader old = SystemReader.getInstance();
        SystemReader.setInstance(new JgitSytemReaderHack(old));

        com.google.common.jimfs.Configuration config = 
                com.google.common.jimfs.Configuration.unix().toBuilder()
                        .setWorkingDirectory("/")
                        .setMaxSize(16L * 1024 * 1024 * 1024)
                        .build();

        final FileSystem fs = Jimfs.newFileSystem(config);
        root = fs.getPath("/");
        
        cloneRepo();
    }
    
    public long getCount() {
        try (Stream<Path> f = Files.list(root.resolve(Configuration.PATH))) {
            return f.count();
        } catch (IOException ex) {}
        
        return 0;
    }

    private void cloneRepo() {
        final int idx = Configuration.BRANCH.lastIndexOf('/');
        final String local = idx < 0 ? Configuration.BRANCH : Configuration.BRANCH.substring(idx + 1);
        
        try (Git g = Git.cloneRepository()
                          .setURI(Configuration.GIT)
                          .setDirectory(root)
                          .setBranch(local)
                          .setRemote(Configuration.BRANCH)
                          .call()) {
        } catch (GitAPIException ex) {
            Logger.getLogger(GitRepoBean.class.getName()).log(Level.SEVERE, ex.getMessage());
            ex.printStackTrace();
        }
        
        last_sync = System.currentTimeMillis();
    }
    
    private void pullRepo() {
        final int idx = Configuration.BRANCH.lastIndexOf('/');
        final String local = idx < 0 ? Configuration.BRANCH : Configuration.BRANCH.substring(idx + 1);
        
        try {
            PullResult r = Git.open(root).pull().setRemote(Configuration.BRANCH).call();
            if (!r.isSuccessful()) {
                Logger.getLogger(GitRepoBean.class.getName()).log(Level.SEVERE, r.toString());
            }
        } catch (GitAPIException ex) {
            Logger.getLogger(GitRepoBean.class.getName()).log(Level.SEVERE, ex.getMessage());
        } catch (IOException ex) {
            Logger.getLogger(GitRepoBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        last_sync = System.currentTimeMillis();
    }

    public DirectoryStream stream() throws IOException {
        if (System.currentTimeMillis() - last_sync > 10000) {
            pullRepo();
        }
        return Files.newDirectoryStream(root.resolve(Configuration.PATH), 
                (Path file) -> Files.isDirectory(file, LinkOption.NOFOLLOW_LINKS));
    }
    
    @PreDestroy
    public void destroy() {
        
    }
}
