/*
 * Copyright (C) 2021 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package es.elixir.bsc.ecotools.frontend.ui.model;

import es.elixir.bsc.ecotools.frontend.repo.GitRepoBean;
import es.elixir.bsc.ecotools.frontend.repo.ToolDirBean;
import es.elixir.bsc.ecotools.frontend.repo.ToolProvider;
import java.io.IOException;
import java.io.Serializable;
import java.nio.file.DirectoryStream;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import org.primefaces.model.FilterMeta;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortMeta;

/**
 * @author Dmitry Repchevsky
 */

@ViewScoped
public class ToolsDataModel extends LazyDataModel<ToolDirBean> 
        implements Serializable {

    @Inject
    private GitRepoBean repo;

    private int count = -1;
    
    private String tool_name_filter;
    private final List<String> tool_type_filters;
    
    public ToolsDataModel() {
        tool_type_filters = new ArrayList();
    }
    
    public String getFilter() {
        return tool_name_filter;
    }
    
    public void setFilter(String text) {
        tool_name_filter = text;
    }
    
    public List<String> getFilters() {
        return tool_type_filters;
    }

    public void setProvider(String provider) {
        if (tool_type_filters.contains(provider)) {
            tool_type_filters.remove(provider);
        } else {
            tool_type_filters.add(provider);
        }
        count = -1;
    }

    public void reset() {
        count = -1;
    }
    
    @Override
    public int getRowCount() {
        if (count < 0) {
            if ((tool_type_filters == null || tool_type_filters.isEmpty()) &&
                (tool_name_filter == null || tool_name_filter.isEmpty())) {
                try (DirectoryStream<Path> stream = repo.stream()) {
                    count = (int)StreamSupport.stream(stream.spliterator(), false).count();
                } catch (IOException ex) {
                    Logger.getLogger(ToolsDataModel.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else {
                try (DirectoryStream<Path> stream = repo.stream()) {
                    count = (int)StreamSupport.stream(stream.spliterator(), false)
                            .map(ToolDirBean::new)
                            .filter(tool -> tool.getName().contains(tool_name_filter))
                            .filter(tool -> tool.allMatch(tool_type_filters))
                            .count();
                } catch (IOException ex) {
                    Logger.getLogger(ToolsDataModel.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return count;
    }

    @Override
    public String getRowKey(ToolDirBean tool) {
        return tool.getName();
    }

    @Override
    public List<ToolDirBean> load(int offset, int pageSize, 
            Map<String, SortMeta> sortBy, Map<String, FilterMeta> filterBy) {
  
        if ((tool_type_filters == null || tool_type_filters.isEmpty()) &&
            (tool_name_filter == null || tool_name_filter.isEmpty())) {
            try (DirectoryStream<Path> stream = repo.stream()) {
                return StreamSupport.stream(stream.spliterator(), false)
                        .skip(Math.min(getRowCount(), offset))
                        .limit(Math.min(getRowCount(), offset + pageSize))
                        .map(ToolDirBean::new)
                        .collect(Collectors.toList());
            } catch (IOException ex) {
                Logger.getLogger(ToolsDataModel.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        try (DirectoryStream<Path> stream = repo.stream()) {
            return StreamSupport.stream(stream.spliterator(), false)
                    .map(ToolDirBean::new)
                    .filter(tool -> tool.getName().contains(tool_name_filter))
                    .filter(tool -> tool.allMatch(tool_type_filters))
                    .skip(Math.min(getRowCount(), offset))
                    .limit(Math.min(getRowCount(), offset + pageSize))
                    .collect(Collectors.toList());
        } catch (IOException ex) {
            Logger.getLogger(ToolsDataModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return Collections.EMPTY_LIST;
    }
    
    public List<String> getProviders() {
        return Arrays.stream(ToolProvider.values()).map(provider -> provider.name()).collect(Collectors.toList());
    }
}
