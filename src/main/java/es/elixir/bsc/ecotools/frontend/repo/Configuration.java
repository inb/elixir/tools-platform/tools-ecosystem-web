/*
 * Copyright (C) 2021 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package es.elixir.bsc.ecotools.frontend.repo;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.nio.file.Paths;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Read configuration from the config.properties file.
 * 
 * @author Dmitry Repchevsky
 */

public final class Configuration {
    
    public final static String GIT;
    public final static String BRANCH;
    public final static String PATH;
    
    public final static String HTTP_PATH;
    
    static {
        String git = null;
        String branch = null;
        String path = null;
        String http_path = null;
        try (InputStream in = Configuration.class.getClassLoader().getResourceAsStream("META-INF/config.properties")) {
            final Properties p = new Properties();
            p.load(in);
            git = p.getProperty("git");
            branch = p.getProperty("branch", "origin/master");
            path = p.getProperty("path");
            
            http_path = URI.create(git.replace(".git", "/tree/")).resolve(Paths.get(branch).getFileName().toString()).toString();
        } catch (IOException ex) {
            Logger.getLogger(Configuration.class.getName()).log(Level.SEVERE, ex.getMessage());
        }
        GIT = git;
        BRANCH = branch;
        PATH = path;
        HTTP_PATH = http_path;
    }
}
